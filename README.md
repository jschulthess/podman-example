# podman-example

## Intro

When looking at current Fedora (and upcomming RHEL 8) platforms you realise that there is no more docker engine.
Instead other container tools take its place. On Redhat platforms this is [*podman*](https://podman.io).

Installation is quite simple.

  * Fedora and RHEL 8:
    ```
    # Fedora and RHEL/CentOS 8
    dnf module install container-tools
    # RHEL 7.6
    subscription-manager repos --enable=rhel-7-server-extras-rpms
    yum -y install podman
    ```

    The package container-tools contains podman and some other useful tools such as [buildah](https://buildah.io) or [skopeo](https://github.com/containers/skopeo)

  * Ubuntu (18.04):
    ```
    get install -y software-properties-common uidmap
    apt-repository -y ppa:projectatomic/ppa
    apt update
    apt -y install podman
    ```

Podman understands all docker CLI commands. So basic usage for handling single containers can be achieved by simply replacing "docker" with "podman" in the command.
On RHEL there is an additional package "podman-docker", which allows transparent usage of "docker" commands. Alternatively an "alias docker=podman" in the bash profile will also work.

So far everything looks familiar except maybe for the fact that there is no more Docker daemon running.

Of course there are more differences including:

  * Podman supports multiple container image formats icluding OCI and Docker image formats.
  * Podman introduces "pod" to manage groups of containers.
  * Podman acts as a CLI to the Container Runtime Interface (CRI) implemented by runc.
  * Podman is a user space tool.

A note: Even though podman can run as non-root most of the things podman does require more privileges than are typically granted to normal users (eg. write access to /var/run/). So for starters we'll use the command as root for this exapmle. For more information on *rootless* mode take a look at the podman man page.

## Multiple Containers

When faced with the question "how to run a docker-compose like multi-container setup?" the concept of "pod" starts to make sense. With podman any container setup is started in a pod.

There is a Python tool called [podman-compose](https://github.com/muayyad-alsadi/podman-compose) which allows to load "docker-compose.yml" using podman. Get the Python script and place it somewhere in the PATH as an executable file.

This project contains some sample files that should convey a feeling for podman:

Prerequisites:
  * clone this repository
  * make sure any docker daemon is stopped
  * to access/pull the base images you may have to edit the list of searched registries in /etc/conainer/registries.conf, eg.
    [registries.search]
    registries = ['docker.io','registry.fedoraproject.org','registry.centos.org']

  1. build an image 
     ```
     cd /path/to/podman-exmple
     podman build .
     ```
  2. load docker-compose.yml
     ```
     # the following loads docker-compose.yml into a single pod
     # see the podman-compose project for other options (eg. one pod per container)
     podman-compose.py -t 1podfw -f docker-compose.yml up
     curl http://localhost:5000
     # Note: if it fails you may have to tag the web image if it wasn't tagged in the build
     # check with
     podman images
     # then do
     podman tag <image_id> docker.io/library/composetest_web:latest
     podman-compose.py -t 1podfw -f docker-compose.yml up
     ```
  3. see some info
     ```
     # list containers
     podman ps
     # list pods
     podman pod ps
     # see runtime usage, processes
     podman pod top <pod-id>, eg.
     podman pod top composetest
     ```
  3. Generate kube yaml. This creates a YAML file that could be loaded into Kubernetes cluster
     ```
     podman generate kube composetest > composetest.yml
     # have a look at the output
     less composetest.yml
     # this file could be used to create the resources in a kubernetes cluster
     (kubectl create -f composetest.yml)
     ```
  4. stop original
     ```
     podman-compose.py -t 1podfw -f docker-compose.yml down
     ```
  5. start from kube yml
     ```
     podman play kube composetest.yml
     ```
  6. stop kube
     ```
     podman pod kill composetest
     ```

## Conclusion

Podman may seem to be a strange change at first when comming from a Docker world.
But it very quickly manifests as a transitioning tool for moving from a Docker dominated container world into a Open container environment where any OCI container image format is treated equal. Containers can be run in stand-alone fassion without any daemon bottle-necks.

Welcome to a more open container world.


